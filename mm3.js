//"use strict";
var fs = require("fs");
var mkdirp = require("mkdirp");
var archiver = require("archiver");
var server = require("http").Server(app); // not sure what this is ?
var configure = require("./router");
var express = require("express");
var request = require("request");
var nodePardot = require("node-pardot");
var bodyParser = require("body-parser");
var rp = require("request-promise");
var app = express();
var port = process.env.PORT || 3331;
var router = express.Router();
var parseString = require("xml2js").parseString;
//configure(router);
var password = "adsjndjsa123@A";
var userkey = "5dbfe038838cc9f32137c10860a81426";
var emailAdmin = "Alessio.Ferri@ClearChannel.co.uk";
var config = require("./config.json");
app.use(bodyParser.urlencoded({ limit: "20mb" }));
app.use(bodyParser.json({ limit: "20mb" }));
app.use(express.static("/public"));
app.use(express.static("/js"));
app.use(express.static("/tmp"));
app.use(express.static("/img"));
app.use(express.static("/css"));
app.get("/mm3", function (req, res) {
  res.sendfile(__dirname + "/index.html");
});
app.listen(port);
console.log("Test server started! At http://localhost:" + port); // Confirms server start
app.post("/mm3/savePhotos", function (req, res) {
  // console.log('here');
  var folder = Math.random().toString(36).substr(2, 20);
  var photos = req.body;
  var counts = 0;
  var callback = function (counts) {
    if (counts < photos.length) {
      saveBase64(photos[counts], folder, counts, callback);
    } else {
      var response = { folder: folder, photos: photos.length };
      res.send(response);
    }
  };
  saveBase64(photos[counts], folder, counts, callback);
});
app.post("/mm3/downloadZip", function (req, res) {
  var photos = req.body.photos;
  var fileName = req.body.fileName;
  var out = photos[0];
  var test = out.split("/");
  var loc = test.pop();
  var end = test.join("/");
  var outName =
    config["source_path"] + end + "/Clear_Channel_" + fileName + "_mockups.zip";
  var output = fs.createWriteStream(outName);
  var archive = archiver("zip", { store: true });
  var zip = function (photos, f) {
    for (var t = 0; t < photos.length; t++) {
      var file = "mockUp" + t + ".jpg";
      var from = config["source_path"] + photos[t];
      archive.file(from, { name: file });
    }
    f();
  };
  output.on("close", function () {
    res.send(end + "/Clear_Channel_" + fileName + "_mockups.zip");
  });
  archive.on("error", function (err) {
    throw err;
  });
  archive.pipe(output);
  zip(photos, f);
  function f() {
    archive.finalize();
  }
});
function saveBase64(photo, folder, counts, callback) {
  var result = photo.split(",")[1];
  var path = "tmp/" + folder;
  var filename = path + "/out" + counts + ".png";
  mkdirp(path, function () {
    fs.writeFile(filename, result, "base64", function (error) {
      if (error) {
      } else {
        counts++;
        callback(counts);
      }
    });
  });
}
//Registeration
function secondFunctionFirst(email_user, first_name, last_name, company) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      // Configure the request
      var headers = {
        "Content-Type": "application/x-www-form-urlencoded",
      };
      var user = {
        url: "https://pi.pardot.com/api/login/version/3",
        method: "POST",
        headers: headers,
        form: {
          user_key: userkey,
          email: emailAdmin,
          password: password,
          format: "json",
          json: true,
        },
      };
      // Start the request
      request(user, function (error, response) {
        if (!error && response.statusCode === 200) {
          console.log(response);
          resolve({
            //Authentication successful
            data_email: email_user,
            data_first_name: first_name,
            data_last_name: last_name,
            data_company: company,
            data_api: response["body"]["api_key"],
          });
        } else {
          console.log("Authentication Failed", error);
        }
      });
    }, 500);
  });
}
function thirdFunctionFirst(result) {
  return new Promise(function () {
    setTimeout(function () {
      // Configure the request
      var api = result[0].data_api;
      var userEmail = result[0].data_email;
      var Fname = result[0].data_first_name;
      var Lname = result[0].data_last_name;
      var company = result[0].data_company;
      var headers = {
        "User-Agent": "Super Agent/0.0.1",
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Pardot api_key=" + api + ", user_key=" + userkey,
      };
      var options = {
        url: "https://pi.pardot.com/api/prospect/version/4/do/create/email",
        method: "POST",
        headers: headers,
        form: {
          email: userEmail,
          first_name: Fname,
          last_name: Lname,
          company: company,
          score: "50",
        },
      };
      // Start the request
      request(options, function (error, response) {
        if (!error && response.statusCode === 200) {
        } else {
        }
      });
    }, 500);
  });
}
app.post("/mm3/api/data", function (req, res) {
  var email_user = req.body.email;
  var first_name = req.body.fname;
  var last_name = req.body.lname;
  var company = req.body.company;
  // console.log(company);
  Promise.all([
    secondFunctionFirst(email_user, first_name, last_name, company),
  ]).then(thirdFunctionFirst);
  res.send(first_name + " " + last_name + " " + email_user);
});
//LOGIN
function firstFunctionSecond() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      // Configure the request
      var headers = {
        "Content-Type": "application/x-www-form-urlencoded",
      };
      var user = {
        url: "https://pi.pardot.com/api/login/version/3",
        method: "POST",
        headers: headers,
        form: {
          user_key: userkey,
          email: emailAdmin,
          password: password,
          format: "json",
          json: true,
        },
      };
      // Start the request
      request(user, function (error, response) {
        if (!error && response.statusCode === 200) {
          var api_key = response["body"]["api_key"];
          console.log(response);
          resolve(
            //Authentication successful
            { data_api: api_key }
          );
        } else {
          console.log("Authentication Failed", error);
        }
      });
    }, 500);
  });
}
app.post("/mm3/back-end/controller", function (req, res) {
  firstFunctionSecond().then(function (result) {
    var login = req.body.LoginEmail;
    var api = result.data_api;
    var headers = {
      "User-Agent": "Super Agent/0.0.1",
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: "Pardot api_key=" + api + ", user_key=" + userkey,
    };
    var apiBody = {
      email: login,
    };
    var options = {
      uri: "https://pi.pardot.com/api/prospect/version/4/do/read",
      method: "POST",
      headers: headers,
      form: apiBody,
      json: true, // Automatically stringifies the body to JSON
    };
    if (login.indexOf("@") > 0) {
      // maybe use node email validation ?
      rp(options)
        .then(function (parsedBody) {
          parseString(parsedBody, function (err, result) {
            // console.dir(result['rsp']['prospect'][0]['id'][0]);
            var newScore =
              Number(result["rsp"]["prospect"][0]["score"][0]) + 50;
            // console.log(api, userkey);
            var toSend = {
              uri:
                "https://pi.pardot.com/api/prospect/version/4/do/update/id/" +
                result["rsp"]["prospect"][0]["id"][0] +
                "?score=" +
                String(newScore),
              headers: {
                Authorization:
                  "Pardot api_key=" + api + ", user_key=" + userkey,
              },
              method: "GET",
            };
            rp(toSend)
              .then(function (parsedBody) {
                // console.log('success');
              })
              .catch(function (err) {
                // console.log('error');
              });
          });
          res.status(200).send({ user: login });
        })
        .catch(function (err) {
          res.status(400).send("fail to login, no such email");
        });
    } else {
      res.status(404).send("Incorrect length");
    }
  });
});
